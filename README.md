
## This project...
**This is a QA challenge proposed by Avenue Code as part of the selective process for a position of QA Automation Engineer**

## Candidate info
**Name:** Alex Candido

**Email:** alexcandido10@gmail.com

**Phone:** (87) 9 9949-4449


## Project instructions
**Language:** Python

**Additional framework:** behave

**IDE:** Pycharm

**Browser:** Chrome 78

**OS:** MacOS Mojave

**Username:** alexcandido10@gmail . com

**Password:** desafioqa

**Config files:**

- behave.ini -> contains the 'browser' property

- environment . py -> contains the 'RELATIVE_PATH' variable pointing to the 'resources' folder where the selenium webdriver (chromedriver) is located.

**How to execute?**

- Go to the _**/AvenueCodeChallenge/tests**_ and execute the following command from terminal _**behave**_. All the tests will be executed. If you want to execute some tests only, add the tag ***@wip*** above the Scenario name at .feature files and run _**behave --tags=wip**_ .


If you want to test in other browsers, download the driver and place it on the 'resources' folder and rename the references to this file in the environment . py file.