from behave import given, when, then
from nose.tools import eq_, assert_false

from pages.tasks_page import TasksPage


@given(u'I click on the add task button')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_add_new_task_button()


@given(u'I fill a new task "{mytask}"')
def step_impl(context, mytask):
    context.page_object = TasksPage(context.driver)
    context.page_object.fill_new_task(mytask)


@given(u'I click on the "Manage Subtasks" button')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_manage_subtasks_button()


@when(u'I click on the "Manage Subtasks" button')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_manage_subtasks_button()


@given(u'I fill a new subtask "{subtask}"')
def step_impl(context, subtask):
    context.page_object = TasksPage(context.driver)
    context.page_object.fill_new_subtask(subtask)


@when(u'I click on the add subtask button')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_add_subtask_button()


@when(u'I click on the close button')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_close_modal_button()


@then(u'The subtask is created: "{subtask}"')
def step_impl(context, subtask):
    context.page_object = TasksPage(context.driver)
    eq_(context.page_object.validate_subtask_on_the_table(subtask), True, )


@then(u'The subtask is not created')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    assert_false(context.page_object.validate_subtask_on_the_table("empty"))


@then(u'The user should see a button "Manage Subtasks" with the number of subtasks "{subtask_amount}" created')
def step_impl(context, subtask_amount):
    context.page_object = TasksPage(context.driver)
    eq_(context.page_object.validate_manage_subtask_message_button(subtask_amount), True, )


@then(u'I cannot edit the task description "{edition}", "{final_string}"')
def step_impl(context, edition, final_string):
    context.page_object = TasksPage(context.driver)
    context.page_object.edit_task(edition)
    context.page_object.click_on_close_modal_button()
    assert_false(context.page_object.validate_new_task(final_string))


@when(u'I fill a new subtask "{subtask}"')
def step_impl(context, subtask):
    context.page_object = TasksPage(context.driver)
    context.page_object.fill_new_subtask(subtask)


@then(u'The subtask "{third_subtask}" is created in the last position of the table')
def step_impl(context, third_subtask):
    context.page_object = TasksPage(context.driver)
    eq_(context.page_object.validate_subtask_on_the_last_position(third_subtask), True, )

