from behave import given, when, then
from nose.tools import assert_false, eq_
from pages.base_page import BasePage
from pages.tasks_page import TasksPage
from pages.user_instructions_page import UserInstructionsPage
from pages.bugs_page import BugsPage
from pages.user_stories_page import UserStoriesPage


@given(u'I am logged in')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    context.page_object.see_confirmation_message()


@given(u'I am on My Tasks page')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    context.page_object.open_tasks_page()


@when(u'I am on User Instructions page')
def step_impl(context):
    context.page_object = UserInstructionsPage(context.driver)
    context.page_object.see_user_instructions_message()


@then(u'I always see "My Tasks" link on the NavBar')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.see_my_tasks_link()


@when(u'I am on User Stories page')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    context.page_object.open_user_stories_page()
    context.page_object = UserStoriesPage(context.driver)
    context.page_object.see_user_stories_message()


@when(u'I am on Bugs page')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    context.page_object.open_bugs_page()
    context.page_object = BugsPage(context.driver)
    context.page_object.see_bugs_message()


@when(u'I am on My Tasks page')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    context.page_object.open_tasks_page()


@then(u'I will be redirected to the tasks list')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.see_my_tasks_todo_list()


@when(u'I click on "My Tasks" link on the NavBar')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_my_tasks_link()


@then('I see a message with the daily tasks for the user "{username}"')
def step_impl(context, username):
    context.page_object = TasksPage(context.driver)
    context.page_object.see_user_tasks_message(username)


@when(u'I fill a new task "{mytask}"')
def step_impl(context, mytask):
    context.page_object = TasksPage(context.driver)
    context.page_object.fill_new_task(mytask)


@when(u'I press enter')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.press_enter()


@when(u'I click on the add task button')
def step_impl(context):
    context.page_object = TasksPage(context.driver)
    context.page_object.click_on_add_new_task_button()


@then(u'A new task is created: "{mytask}')
def step_impl(context, mytask):
    context.page_object = TasksPage(context.driver)
    context.page_object.validate_new_task(mytask)


@then(u'The task is not created: "{mytask}"')
def step_impl(context, mytask):
    context.page_object = TasksPage(context.driver)
    assert_false(context.page_object.validate_new_task(mytask))

