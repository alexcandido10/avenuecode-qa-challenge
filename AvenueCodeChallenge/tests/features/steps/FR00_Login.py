from pages.base_page import BasePage
from pages.login_page import LoginPage
from behave import given, when, then


@given(u'I am in the login page')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    context.page_object.open(context.url)
    context.page_object = LoginPage(context.driver)
    context.page_object.wait_page_to_load()


@when(u'I fill the "{data}" username')
def step_impl(context, data):
    context.page_object.fill_username(data)


@when(u'I fill the "{data}" password')
def step_impl(context, data):
    context.page_object.fill_password(data)


@when(u'I click on "Sign in" button')
def step_impl(context):
    context.page_object.click_sing_in_button()


@then(u'I should see the Signed in successfully message')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    assert (context.page_object.see_confirmation_message())


@then(u'I should see QA Assessment message')
def step_impl(context):
    context.page_object = BasePage(context.driver)
    assert (context.page_object.see_qa_assessment_message())
