
def handle_exception(function_name, err):
    print(err.__name__ + " has been thrown. " + function_name.__name__.upper() + " failed")