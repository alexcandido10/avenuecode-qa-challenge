import time

from selenium.common.exceptions import TimeoutException
from pages.base_page import BasePage
from common.helper_function import handle_exception
from selenium.webdriver.common.by import By


class TasksPage(BasePage):

    welcome_user_string = (By.XPATH, '//*[contains(text(),"Welcome")]')

    def see_welcome_message(self):
        try:
            self.wait_present(self.welcome_user_string)
        except TimeoutException:
            handle_exception(self.see_welcome_message, TimeoutException)
            return False
        return True

    my_tasks_link = (By.XPATH, '//*[@id="my_task"]')

    def see_my_tasks_link(self):
        try:
            self.wait_present(self.my_tasks_link)
        except TimeoutException:
            handle_exception(self.see_my_tasks_link, TimeoutException)
            return False
        return True

    todo_list_string = (By.XPATH, '//h1[contains(text(),"ToDo List")]')

    def see_my_tasks_todo_list(self):
        try:
            self.wait_present(self.todo_list_string)
        except TimeoutException:
            handle_exception(self.see_my_tasks_todo_list, TimeoutException)

    def click_on_my_tasks_link(self):
        self.click_element(self.my_tasks_link)

    def see_user_tasks_message(self, username):

        user_tasks_list_string = (By.XPATH, '//h1[contains(text(), "user\'s ToDo List")]'.replace("user", username))

        try:
            self.wait_present(user_tasks_list_string)
        except TimeoutException:
            handle_exception(self.see_user_tasks_message, TimeoutException)
            return False
        return True

    new_task_input = (By.XPATH, '//input[@id="new_task"]')

    def fill_new_task(self, task):
        self.fill(task, *self.new_task_input)
        self.implicitly_wait(3)

    def press_enter(self):
        self.enter(*self.new_task_input)
        self.implicitly_wait(3)

    def validate_new_task(self, task):

        task_on_table = (By.XPATH, '//a[contains(@class,"editable") and (text()="taskname")]'.replace("taskname", task))

        try:
            self.wait_present(task_on_table)
        except TimeoutException:
            handle_exception(self.validate_new_task, TimeoutException)
            return False
        return True

    def validate_task_is_not_created(self, task):

        task_on_table = (By.XPATH, '//a[contains(@class,"editable") and (text()="taskname")]'.replace("taskname", task))

        try:
            self.wait_present(task_on_table)
        except TimeoutException:
            handle_exception(self.validate_task_is_not_created, TimeoutException)
            return False
        return True

    @staticmethod
    def implicitly_wait(sec):
        time.sleep(sec)

    add_new_task_button = (By.XPATH, '//span[contains(@ng-click,"addTask")]')

    def click_on_add_new_task_button(self):
        self.click_element(self.add_new_task_button)
        self.implicitly_wait(3)

    see_manage_subtasks_button_path = (By.XPATH, '//button[contains(text(),"Manage Subtasks")]')

    def click_on_manage_subtasks_button(self):
        self.click_element(self.see_manage_subtasks_button_path)
        self.implicitly_wait(3)

    def see_manage_subtasks_button(self):
        try:
            self.wait_present(self.see_manage_subtasks_button_path)
        except TimeoutException:
            handle_exception(self.see_manage_subtasks_button, TimeoutException)
            return False
        return True

    new_subtask_input = (By.XPATH, '//input[@id="new_sub_task"]')

    def fill_new_subtask(self, subtask):
        self.fill(subtask, *self.new_subtask_input)
        self.implicitly_wait(3)

    add_subtask_button = (By.XPATH, '//button[@id="add-subtask"]')

    def click_on_add_subtask_button(self):
        self.click_element(self.add_subtask_button)
        self.implicitly_wait(3)

    close_modal_button = (By.XPATH, '//button[contains(text(),"Close")]')

    def click_on_close_modal_button(self):
        self.click_element(self.close_modal_button)
        self.implicitly_wait(3)

    def validate_manage_subtask_message_button(self, subtask_amount):

        manage_subtask_message = (By.XPATH, '//button[contains(text(),"(subtask_amount) Manage Subtasks")]'.replace("subtask_amount", subtask_amount))

        try:
            self.wait_present(manage_subtask_message)
            self.implicitly_wait(3)
        except TimeoutException:
            handle_exception(self.validate_manage_subtask_message_button, TimeoutException)
            return False
        return True

    def validate_subtask_on_the_table(self, subtask):
        subtask_on_the_table = (By.XPATH, '//a[text()="subtask"]'.replace("subtask", subtask))
        try:
            self.implicitly_wait(3)
            self.wait_present(subtask_on_the_table)
            self.implicitly_wait(3)
        except TimeoutException:
            handle_exception(self.validate_subtask_on_the_table, TimeoutException)
            return False
        return True

    edit_task_textarea = (By.XPATH, '//textarea[@id="edit_task"]')

    def edit_task(self, task):
        self.implicitly_wait(3)
        self.fill(task, *self.edit_task_textarea)
        self.implicitly_wait(3)

    edit_task_id = (By.XPATH, '//h3[contains(@class,"modal-title")]')

    def edit_task_id(self, task):
        self.fill(task, *self.edit_task_id)

    def get_task_id(self):
        return self.get_value(*self.edit_task_id).get_text()

    def validate_subtask_on_the_last_position(self, subtask):
        third_position_on_subtask_table = (By.XPATH, '//tr[3]//td[2]//a[(text()="subtask")]'.replace("subtask", subtask))
        try:
            self.implicitly_wait(3)
            self.wait_present(third_position_on_subtask_table)
            self.implicitly_wait(3)
        except TimeoutException:
            handle_exception(self.validate_subtask_on_the_last_position, TimeoutException)
            return False
        return True
