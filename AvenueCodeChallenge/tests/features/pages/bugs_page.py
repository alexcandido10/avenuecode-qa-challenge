from selenium.common.exceptions import TimeoutException
from common.helper_function import handle_exception
from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class BugsPage(BasePage):

    bugs_string = (By.XPATH, '//div[contains(.,"Bugs I have found so far")]')

    def see_bugs_message(self):
        try:
            self.wait_present(self.bugs_string)
        except TimeoutException:
            handle_exception(self.see_bugs_message, TimeoutException)
            return False
        return True
