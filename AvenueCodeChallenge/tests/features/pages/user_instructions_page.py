from selenium.common.exceptions import TimeoutException
from pages.base_page import BasePage
from common.helper_function import handle_exception
from selenium.webdriver.common.by import By


class UserInstructionsPage(BasePage):

    user_instructions_string = (By.XPATH, '//h1[contains(text(), "User Instructions")]')

    def see_user_instructions_message(self):
        try:
            self.wait_present(self.user_instructions_string)
        except TimeoutException:
            handle_exception(self.see_user_instructions_message, TimeoutException)
            return False
        return True
