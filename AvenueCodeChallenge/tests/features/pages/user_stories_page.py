from pages.base_page import BasePage
from selenium.common.exceptions import TimeoutException
from common.helper_function import handle_exception
from selenium.webdriver.common.by import By


class UserStoriesPage(BasePage):

    user_stories_string = (By.XPATH, '//h1[contains(text(),"User Stories")]')

    def see_user_stories_message(self):
        try:
            self.wait_present(self.user_stories_string)
        except TimeoutException:
            handle_exception(self.see_user_stories_message, TimeoutException)
            return False
        return True
