import time

from pages.base_page import BasePage
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from common.helper_function import handle_exception


class LoginPage(BasePage):

    username = (By.XPATH, '//input[@id="user_email"]')
    password = (By.XPATH, '//input[@id="user_password"]')
    sign_in_button = (By.XPATH, '//input[@name="commit"]')

    def fill_username(self, value):
        self.fill(value, *self.username)

    def fill_password(self, value):
        self.fill(value, *self.password)

    def click_sing_in_button(self):
        self.implicitly_wait(0.5)
        self.click_element(self.sign_in_button)

    def wait_page_to_load(self):
        try:
            self.wait_present(self.sign_in_button)
            self.wait_clickable(self.sign_in_button)
        except TimeoutException:
            handle_exception(self.wait_page_to_load, TimeoutException)

    @staticmethod
    def implicitly_wait(sec):
        time.sleep(sec)
