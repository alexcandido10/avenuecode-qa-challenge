from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException
from common.helper_function import handle_exception
from selenium.webdriver.common.keys import Keys


class BasePage(object):
    __TIMEOUT = 5

    confirmation_text = (By.XPATH, '//div[contains(text(),"Signed in successfully.")]')

    def __init__(self, browser):
        self.browser = browser
        self._web_driver_wait = WebDriverWait(browser, BasePage.__TIMEOUT)

    def open(self, url):
        self.browser.get(url)

    def find_element(self, *locator):
        return self.browser.find_element(*locator)

    def click_element(self, locator):
        try:
            element = self.wait_clickable(locator)
        except TimeoutException:
            handle_exception(self.click_element, TimeoutException)
            raise TimeoutException
        element.click()

    def fill(self, text, *locator):
        self.find_element(*locator).send_keys(text)

    def wait_visible(self, locator):
        return self._web_driver_wait.until(ec.visibility_of_element_located(locator))

    def wait_present(self, locator):
        return self._web_driver_wait.until(ec.presence_of_element_located(locator))

    def wait_clickable(self, locator):
        return self._web_driver_wait.until(ec.element_to_be_clickable(locator))

    def wait_page_to_load(self):
        try:
            self.wait_present(self.sign_in_button)
            self.wait_clickable(self.sign_in_button)
        except TimeoutException:
            handle_exception(self.wait_page_to_load, TimeoutException)

    def see_confirmation_message(self):
        try:
            self.wait_present(self.confirmation_text)
        except TimeoutException:
            handle_exception(self.see_confirmation_message, TimeoutException)
            return False
        return True

    qa_assessment_string = (By.XPATH, '//h1[contains(text(),"QA Assessment")]')

    def see_qa_assessment_message(self):
        try:
            self.wait_present(self.qa_assessment_string)
        except TimeoutException:
            handle_exception(self.see_qa_assessment_message, TimeoutException)
            return False
        return True

    bugs_url = 'https://qa-test.avenuecode.com/bugs'

    def open_bugs_page(self):
        self.open(self.bugs_url)

    user_stories_url = 'https://qa-test.avenuecode.com/user-stories'

    def open_user_stories_page(self):
        self.open(self.user_stories_url)

    tasks_url = 'https://qa-test.avenuecode.com/tasks'

    def open_tasks_page(self):
        self.open(self.tasks_url)

    def enter(self, *locator):
        self.find_element(*locator).send_keys(Keys.ENTER)

    def get_value(self, *locator):
        return self.find_element(*locator)
