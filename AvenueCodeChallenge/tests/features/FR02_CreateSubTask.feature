Feature: Create SubTask
  As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks into smaller pieces


  Scenario Outline: The user should be able to create a subtask
    Given I am on My Tasks page
    And I fill a new task "My Task"
    And I click on the add task button
    And I click on the "Manage Subtasks" button
    And I fill a new subtask "<subtask>"
    When I click on the add subtask button
    Then The subtask is created: "<subtask>"

  Examples:
    |subtask    |
    |sub1       |

  @wip
  Scenario: The user should not be able to create an empty subtask
    Given I am on My Tasks page
    And I fill a new task "My Task"
    And I click on the add task button
    And I click on the "Manage Subtasks" button
    And I fill a new subtask "first subtask"
    When I click on the add subtask button
    And I fill a new subtask "second subtask"
    And I click on the add subtask button
    And I fill a new subtask "third subtask"
    And I click on the add subtask button
    Then The subtask "third subtask" is created in the last position of the table


  Scenario: The added subtasks are appended on the bottom part of the modal dialog
    Given I am on My Tasks page
    And I fill a new task "My first"
    And I click on the add task button
    And I click on the "Manage Subtasks" button
    When I click on the add subtask button
    Then The subtask is not created

  Scenario Outline: The user should be able to create a subtask with more than 250 characters
    Given I am on My Tasks page
    And I fill a new task "My Task"
    And I click on the add task button
    And I click on the "Manage Subtasks" button
    And I fill a new subtask "<subtask>"
    When I click on the add subtask button
    Then The subtask is created: "<subtask>"

  Examples:
    |subtask    |
    |This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.       |

  Scenario Outline: The user should see a button labeled as "Manage Subtasks" with have the number of subtasks created
    Given I am on My Tasks page
    And I fill a new task "My task"
    And I click on the add task button
    And I click on the "Manage Subtasks" button
    And I fill a new subtask "subtask 1"
    When I click on the add subtask button
    And I click on the close button
    Then The user should see a button "Manage Subtasks" with the number of subtasks "<subtask_amount>" created

  Examples:
    |subtask_amount |
    |1              |

  Scenario: Make sure the task description is read only
    Given I am on My Tasks page
    And I fill a new task "My original task"
    And I click on the add task button
    When I click on the "Manage Subtasks" button
    Then I cannot edit the task description "Edited", "My original taskEdited"

