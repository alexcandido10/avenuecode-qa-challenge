Feature: Create Task
  As a ToDo App user
  I should be able to create a task
  So I can manage my tasks

  Scenario: Redirect the user to the tasks list
    Given I am logged in
    When I click on "My Tasks" link on the NavBar
    Then I will be redirected to the tasks list
    And I always see "My Tasks" link on the NavBar

  Scenario: Make sure 'My Tasks' link is always on the NavBar of User Instructions page
    Given I am logged in
    When I am on User Instructions page
    Then I always see "My Tasks" link on the NavBar

  Scenario: Make sure 'My Tasks' link is always on the NavBar of User Stories page
    Given I am logged in
    When I am on User Stories page
    Then I always see "My Tasks" link on the NavBar

  Scenario: Make sure 'My Tasks' link is always on the NavBar of Bugs page
    Given I am logged in
    When I am on Bugs page
    Then I always see "My Tasks" link on the NavBar

  Scenario Outline: Make sure the User name is shown
    Given I am logged in
    When I am on My Tasks page
    Then I see a message with the daily tasks for the user "<username>"

  Examples:
    |username|
    |Alex    |

  Scenario Outline: Insert a new task by hitting enter
    Given I am on My Tasks page
    When I fill a new task "<mytask>"
    And I press enter
    Then A new task is created: "<mytask>"

  Examples:
    |mytask|
    |hitting enter|

  Scenario Outline: Insert a new task by clicking on the add task button
    Given I am on My Tasks page
    When I fill a new task "<mytask>"
    And I click on the add task button
    Then A new task is created: "<mytask>"

  Examples:
    |mytask|
    |clicking on add button|


  Scenario Outline: Make sure the task cannot have less than three characters
    Given I am on My Tasks page
    When I fill a new task "<mytask>"
    And I click on the add task button
    Then The task is not created: "<mytask>"

  Examples:
    |mytask|
    |zz|


  Scenario Outline: Make sure the task cannot have more than 250 characters
    Given I am on My Tasks page
    When I fill a new task "<mytask>"
    And I click on the add task button
    Then The task is not created: "<mytask>"

  Examples:
    |mytask|
    |This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.This message contains more than 250 characters.|