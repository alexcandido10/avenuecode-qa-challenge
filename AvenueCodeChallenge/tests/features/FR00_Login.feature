Feature: Log in in the system

  @wip
  Scenario Outline: Perform the log in in the system
    Given I am in the login page
    When I fill the "<username>" username
    And I fill the "<password>" password
    And I click on "Sign in" button
    Then I should see the Signed in successfully message
    And I should see QA Assessment message

    Examples:
    |username                 | password  |
    |alexcandido10@gmail.com  | desafioqa |