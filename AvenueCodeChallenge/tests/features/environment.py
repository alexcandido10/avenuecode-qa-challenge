# -- FILE: features/environment.py
from selenium import webdriver
import os

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
RELATIVE_PATH = "resources"


def before_all(context):
    # environment
    context.browser = context.config.userdata['browser']
    context.chrome_driver_path = os.path.join(PROJECT_ROOT, RELATIVE_PATH, "chromedriver")
    context.geckodriver_path = os.path.join(PROJECT_ROOT, RELATIVE_PATH, "geckodriver")
    context.url = context.config.userdata['main_url']

    # Expand browsers in the future
    if context.browser.lower() == "chrome":
        context.driver = webdriver.Chrome(context.chrome_driver_path)
    elif context.browser.lower() == "firefox":
        context.driver = webdriver.Firefox(executable_path=context.geckodriver_path,
                                           log_path=os.path.join(PROJECT_ROOT, RELATIVE_PATH, "geckodriver.log"))
    else:
        raise AttributeError("Invalid browser!")


def after_all(context):
    context.driver.close()
